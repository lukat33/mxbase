#!/usr/bin/env node

const path = require('path');
const express = require('express');
const bodyParser = require('body-parser')

const solid = require('solid-server');
const signing = require('./vc/api');

let config;
try {
  config = require(path.join(process.cwd(), 'site/index.js'));
} catch (e) {
  throw Error('Your site is missing or has a malformed conf/index.js Please see the README');
}

const app = express();

app.use(bodyParser.json())

app.use('/app/assets/', express.static('site/assets'));
app.use('/app/keys/', express.static('site/keys'));
app.get('/app', function (req, res) {
  res.redirect('/')
});
app.get('/app/config', function (req, res) {
  res.json(config);
});
app.post('/app/sign', async function (req, res) {
  const signed = signing.api.sign(req.body.credential);
  res.json(signed);
});
app.get('/app/verify', async function (req, res) {
  const verified = await signing.api.verify(signed);
  res.json(verified);
});
app.use('/', solid(config.solidSettings));

app.listen(2020, function () {
  console.log('app started on port 2020');
});
