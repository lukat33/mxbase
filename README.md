
# mx (working name)

Designed to be a base for open-ended Express/Solid apps while the world figures out these details properly.

** WIP WIP WIP WIP **

Please contact me if you'd like to help this be a general project. 

Each site instance is separate from mxbase, so multiple sites can be managed with minimal customization per site.

To work properly, you will need to set up a front-end server, see the NGINX section.

It has a rudimentary express route to sign and verify verifiable credentials using vc-js.

For signing, you'll need a key first (see generate-key below).

In this folder:

`npm install`

then

`npm link`

Get a wildcard certificate for your domain.

Then create your instance in a new directory, with at least a site/index.js that has something like

```
module.exports = {
  pages: {},
  base: '/public/(overview)/',
  index: 'TOC',
  display: {},
  solidSettings = {
    multiuser: true,
    email: {
      host: 'mail.your.domain',
      port: 25,
      secure: false,
      tls: {
        rejectUnauthorized: false
      }
    },
    webid: true,
    sslCert: path.resolve('path.to.your.cert'),
    sslKey: path.resolve('path.to.your.key'),
    root: './fs',
    serverUri: 'https://solid.your.domain')
  }
}
```

In the new directory, do this:

* `npm link mxbase`
* `node node_modules/mxbase/bin/generate-key.js`
* `npm node_modules/mxbase/bin/test-sign.js` (optional, do a test sign)


Then start it with something like `DEBUG="solid:*" nodemon node_modules/.bin/base`

## NGINX setup


Create a site config like this:

```
map $http_upgrade $connection_upgrade {
    default upgrade;
      ''      close;
}

server {
  listen 80;
  server_name     your.domain *.your.domain;
  return 301 https://$host$request_uri;
}

server {
  server_name     your.domain *.your.domain;
  error_page 404 /;

  listen          443;
  ssl             on;

  default_type  application/octet-stream;
  sendfile        on;
  keepalive_timeout  65;

  gzip on;
  gzip_comp_level 6;
  gzip_vary on;
  gzip_min_length  1000;
  gzip_proxied any;
  gzip_buffers 16 8k;

  ssl_protocols        TLSv1 TLSv1.1 TLSv1.2;
  ssl_ciphers RC4:HIGH:!aNULL:!MD5;
  ssl_prefer_server_ciphers on;
  ssl_session_cache    shared:SSL:10m;
  ssl_session_timeout  10m;

  ssl_certificate      path.to.your.cert;
  ssl_certificate_key  path.to.your.key;

  location  /app/ {
          proxy_pass http://your.backend.host:2020;
          include "conf.d/proxy";
  }
  location  / {
          proxy_pass http://your.backend.host:2020;
          include "conf.d/proxy";
  }
  location  /a/ {
          proxy_pass http://your.backend.host:2019;
          include "conf.d/proxy";
  }
  location  /static/ {
          proxy_pass http://your.backend.host:2019;
          include "conf.d/proxy";
  }
  location  /sockjs-node/ {
          proxy_pass http://your.backend.host:2019;
          include "conf.d/proxy";
  }
}

```

conf.d/proxy looks like this:

```
proxy_buffering off;
proxy_redirect     off;
proxy_set_header Host $host;
proxy_set_header X-Real-IP $remote_addr;
proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
proxy_set_header Connection keep-alive;
```

