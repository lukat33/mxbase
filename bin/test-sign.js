const api = require('../vc/api');

testSign();

async function testSign() {

    const cred = {
        "@context": [
            "https://www.w3.org/2018/credentials/v1",
            "https://www.w3.org/2018/credentials/examples/v1"
        ],
        "id": "http://example.edu/credentials/1872",
        "type": ["VerifiableCredential", "AlumniCredential"],
        "issuer": "https://example.edu/issuers/565049",
        "issuanceDate": "2010-01-01T19:23:24Z",
        "credentialSubject": {
            "id": "did:example:ebfeb1f712ebc6f1c276e12ec21",
            "alumniOf": "<span lang='en'>Example University</span>"
        }
    }


    const signed = await api.sign(JSON.stringify(cred));
    console.log('signed', signed);

    const verified = await api.verify(signed);

    console.log('verify', verified);
}