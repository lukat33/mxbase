const fs = require('fs');

const key = require('../vc/key');

generateKey();

async function generateKey() {
    ['private', 'site', 'site/keys'].forEach(dir => {
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }
    });
    const result = await key.generate('https://vc.solid.nicer.info/app/keys/', 'ed25519');
    console.log('keygen', result);
    const { privateKeyBase58, controller } = result;
    fs.writeFileSync(`./private/signing.json`, JSON.stringify({ privateKeyBase58, controller }, null, 2));
}
