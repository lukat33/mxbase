/*
 * Based on a sample by Digital Bazaar.
*/
'use strict';

const { Ed25519KeyPair } = require('jsonld-signatures');
const FileWriter = require('./FileWriter');
const Secp256k1KeyPair = require('secp256k1-key-pair');

exports.generate = async (base, keyType) => {
  let k;
  switch (keyType) {
    case 'ed25519':
      k = await Ed25519KeyPair.generate();
      break;
    case 'secp256k1':
      k = await Secp256k1KeyPair.generate();
      break;
    default:
      throw new Error(`Unknown key type: "${keyType}".`);
  }

  const controllerWriter = new FileWriter(base, 'creator.json');
  const keyWriter = new FileWriter(base, 'key.json');

  const publicKeyDoc = Object.assign(k.publicNode(), {
    '@context': 'https://w3id.org/security/v2',
    id: keyWriter.rawUrl,
    controller: controllerWriter.rawUrl
  });
  const controllerDoc = {
    '@context': 'https://w3id.org/security/v2',
    id: controllerWriter.rawUrl,
    assertionMethod: [keyWriter.rawUrl]
  };

  await Promise.all([
    controllerWriter.create({ content: controllerDoc }),
    keyWriter.create({ content: publicKeyDoc }),
  ]);

  return Object.assign(k, {
    id: keyWriter.rawUrl,
    controller: controllerWriter.rawUrl,
  });
};
