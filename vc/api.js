/*
 * Based on a sample by Digital Bazaar.
*/

const fs = require('fs');
const vc = require('vc-js');
const { Ed25519KeyPair, suites: { Ed25519Signature2018 } } = require('jsonld-signatures');
const EcdsaSepc256k1Signature2019 = require('ecdsa-secp256k1-signature-2019');
const Secp256k1KeyPair = require('secp256k1-key-pair');

const documentLoader = require('./document-loader');

const sign = async (credentialRaw) => {
  const keyRaw = fs.readFileSync('./site/keys/key.json');
  const signing = JSON.parse(fs.readFileSync('./private/signing.json'));
  const { privateKeyBase58, controller } = signing;
  const signed = await issue(keyRaw, credentialRaw, privateKeyBase58, controller);
  return signed;
}
const sign2 = async (credentialRaw) => {
  const keyRaw = fs.readFileSync('./site/keys/key.json');
  const signing = JSON.parse(fs.readFileSync('./private/signing.json'));
  const { privateKeyBase58, controller } = signing;
  const suite = getSuite(keyRaw, privateKeyBase58);
  const signed = await issueWithSuite(suite, credentialRaw, controller);
  return signed;
}

const getSuite = async (keyRaw, privateKeyBase58) => {
  try {
    let keyJson;
    try {
      keyJson = JSON.parse(keyRaw.toString());
      keyJson.privateKeyBase58 = privateKeyBase58;
    } catch (e) {
      throw new Error(`Could not parse the key : ${keyRaw}`);
    }

    const { /*controller,*/ type } = keyJson;
    // const issuer = controller;

    let suite;
    switch (type) {
      case 'EcdsaSecp256k1VerificationKey2019':
        suite = new EcdsaSepc256k1Signature2019({
          key: new Secp256k1KeyPair(keyJson),
          verificationMethod: keyJson.id
        });
        break;
      case 'Ed25519VerificationKey2018':
        suite = new Ed25519Signature2018({
          key: new Ed25519KeyPair(keyJson),
          verificationMethod: keyJson.id
        });
        break;
      default:
        throw new Error(`Unknown key type ${type}.`);
    }
    return suite;
  } catch (e) {
    throw (e);
  }
};

const issueWithSuite = async (suite, credentialRaw, controller) => {
  let result;
  try {
    let credentialJson;
    try {
      credentialJson = JSON.parse(credentialRaw);
    } catch (e) {
      console.error('Invalid', credentialRaw);
      throw new Error('Invalid credential.');
    }

    credentialJson.issuer = controller;
    console.log('suite', suite, credentialJson);
    result = await vc.issue({
      credential: credentialJson,
      suite,
      documentLoader,
    });
  } catch (e) {
    throw (e);
  }
  return result;
};

const issue = async (keyRaw, credentialRaw, privateKeyBase58, controller) => {
  let result;
  try {

    let keyJson;
    try {
      keyJson = JSON.parse(keyRaw.toString());
      keyJson.privateKeyBase58 = privateKeyBase58;
    } catch (e) {
      throw new Error(`Could not parse the key : ${keyRaw}`);
    }

    let credentialJson;
    try {
      credentialJson = JSON.parse(credentialRaw);

    } catch (e) {
      console.error('Invalid', credentialRaw);
      throw new Error('Invalid credential.');
    }

    const { /*controller,*/ type } = keyJson;
    // const issuer = controller;

    let suite;
    switch (type) {
      case 'EcdsaSecp256k1VerificationKey2019':
        suite = new EcdsaSepc256k1Signature2019({
          key: new Secp256k1KeyPair(keyJson),
          verificationMethod: keyJson.id
        });
        break;
      case 'Ed25519VerificationKey2018':
        suite = new Ed25519Signature2018({
          key: new Ed25519KeyPair(keyJson),
          verificationMethod: keyJson.id
        });
        break;
      default:
        throw new Error(`Unknown key type ${type}.`);
    }

    credentialJson.issuer = controller;
    result = await vc.issue({
      credential: credentialJson,
      suite,
      documentLoader,
    });
  } catch (e) {
    throw (e);
  }
  return result;
};

const verify = async (signed) => {
  try {
    const credential = signed;
    const result = await vc.verify({
      credential,
      suite: [new Ed25519Signature2018(), new EcdsaSepc256k1Signature2019()],
      documentLoader,
    });
    return result;
  } catch (e) {
    throw (e);
  }
};

module.exports = { sign, issue, verify };
