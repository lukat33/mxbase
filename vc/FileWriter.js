/*
 * Based on a sample by Digital Bazaar.
*/
const fs = require('fs');
const path = require('path');

class FileWriter {
  constructor(base, filename) {
    this.filename = filename;
    this.base = base;
    this.fullRawUrl = this.base + filename;
  }

  get rawUrl() {
    return this.fullRawUrl;
  }

  async create({ content }) {
    const out = path.join('./site/keys', this.filename);
    fs.writeFileSync(out, JSON.stringify(content), null, 2);
    console.log('wrote', out, content);
  }

  async update({ content }) {
    return this.create({ filename: this.filename, content })
  }
}

module.exports = FileWriter;
